-- name: GetPawns :many
SELECT sqlc.embed(pawn), sqlc.embed(pawn_item)
    FROM pawn
        LEFT JOIN pawn_item ON pawn_item.pawn_id = pawn.id
    WHERE customer_id = $1;

-- name: GetPawn :one
SELECT *
    FROM pawn
    WHERE customer_id = $1 AND id = $2;

-- name: GetPawnItemsByPawnID :many
SELECT * FROM pawn_item where pawn_id = $1;

-- name: CreatePawn :exec
INSERT INTO pawn (id, customer_id, date_in, date_out, forfeit_date, renewal_count)
    VALUES ($1, $2, $3, $4, $5, $6);

-- name: CreatePawnItem :exec
INSERT INTO pawn_item (id, pawn_id, description, make, model, serial_number, pawn_amount, fee, total)
    VALUES ($1, $2, $3, $4, $5, $6,  $7, $8, $9);

-- name: GetCustomers :many
SELECT * FROM customer;

-- name: GetCustomer :one
SELECT * FROM customer WHERE id = $1;

-- name: CreateCustomer :exec
INSERT INTO customer (id, first_name, middle_name, last_name, address, phone, dob, drivers_license, drivers_license_exp_date, ssn, sex, race, height, weight)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14);

-- name: GetBrokers :many
SELECT * FROM broker;

-- name: GetBroker :one
SELECT * FROM broker WHERE id = $1;

-- name: CreateBroker :exec
INSERT INTO broker (id, name, fee_rate, email, password, encryption_key) VALUES ($1, $2, $3, $4, $5, $6);

-- name: GetBrokerByEmail :one
SELECT * FROM broker WHERE email = $1;