-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS broker (
    id VARCHAR NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL,
    encryption_key BYTEA NOT NULL,
    fee_rate NUMERIC DEFAULT 0,
    email VARCHAR NOT NULL,
    password BYTEA NOT NULL,
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW(),
    deleted_at TIMESTAMPTZ
);

CREATE TABLE IF NOT EXISTS customer (
    id VARCHAR NOT NULL PRIMARY KEY,
    first_name VARCHAR(128) NOT NULL,
    middle_name VARCHAR(128),
    last_name VARCHAR(128) NOT NULL,
    address JSONB NOT NULL,
    phone VARCHAR(10) NOT NULL,
    email VARCHAR NOT NULL,
    dob DATE NOT NULL,
    drivers_license VARCHAR(256) NOT NULL,
    drivers_license_exp_date TIMESTAMPTZ NOT NULL,
    ssn BYTEA NOT NULL UNIQUE,
    sex VARCHAR(10),
    race VARCHAR(64),
    height NUMERIC,
    weight NUMERIC,
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS pawn (
    id VARCHAR NOT NULL PRIMARY KEY,
    customer_id VARCHAR NOT NULL REFERENCES customer(id),
    date_in TIMESTAMPTZ NOT NULL,
    date_out TIMESTAMPTZ NOT NULL,
    forfeit_date TIMESTAMPTZ NOT NULL,
    renewal_count INT DEFAULT 0,
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS pawn_item (
    id VARCHAR NOT NULL PRIMARY KEY,
    pawn_id VARCHAR NOT NULL REFERENCES pawn(id),
    description TEXT,
    make VARCHAR(128) NOT NULL,
    model VARCHAR(256) NOT NULL,
    serial_number VARCHAR(256) NOT NULL,
    pawn_amount NUMERIC NOT NULL,
    fee NUMERIC NOT NULL,
    total NUMERIC NOT NULL,
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS pawn_item;

DROP TABLE IF EXISTS pawn;

DROP TABLE IF EXISTS customer;
-- +goose StatementEnd
