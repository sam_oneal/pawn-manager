MAIN_PACKAGE_PATH := ./cmd/pawn_manager
BINARY_NAME := pawn_manager
BUILD_OUTPUT := ./bin/${BINARY_NAME}
DOPPLER_PROJECT := pawn-manager
DOPPLER_CONFIG := dev_personal
DOPPLER_RUN_COMMAND := doppler run -p ${DOPPLER_PROJECT} -c ${DOPPLER_CONFIG}

# ==================================================================================== #
# HELPERS
# ==================================================================================== #

## help: print this help message
.PHONY: help
help:
	@echo 'Usage:'
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' | sed -e 's/^/ /'

.PHONY: confirm
confirm:
	@echo -n 'Are you sure? [y/N] ' && read ans && [ $${ans:-N} = y ]

.PHONY: no-dirty
no-dirty:
	git diff --exit-code

## sql-generate: generates the sql code from migration scripts
.PHONY: sql-generate
sql-generate:
	sqlc generate

.PHONY: mock
mock:
	go generate -v ./...

# ==================================================================================== #
# QUALITY CONTROL
# ==================================================================================== #

## tidy: format code and tidy mod file
.PHONY: tidy
tidy:
	go fmt ./...
	go mod tidy -v

## audit: run quality control checks
.PHONY: audit
audit:
	go mod verify
	go vet ./...
	go run honnef.co/go/tools/cmd/staticcheck@latest --checks=all,-ST1000,-U1000 ./...
	go run golang.org/x/vuln/cmd/govulncheck@latest ./...
	go test -race -buildvcs -vet=off ./...

# ==================================================================================== #
# DEVELOPMENT
# ==================================================================================== #

## clean: clean up any crated binaries
.PHONY: clean
clean:
	rm -rf $(BUILD_OUTPUT)

## test: run all tests
.PHONY: test
test:
	go test -v -race -buildvcs ./...

## test/cover: run all tests and display coverage
.PHONY: test/cover
test/cover:
	go test -v -race -buildvcs -coverprofile=/tmp/coverage.out ./...
	go tool cover -html=/tmp/coverage.out

## build: build the application
.PHONY: build
build: clean tidy # sql-generate
	# Include additional build steps like Typescript, SCSS or Tailwind compilation here
	go build -o=${BUILD_OUTPUT} ${MAIN_PACKAGE_PATH}/


## run: run the application
.PHONY: run
run: build
	${DOPPLER_RUN_COMMAND} -- ${BUILD_OUTPUT}


## run/live: run the application with reloading on file changes
.PHONY: run/live
run/live:
	${DOPPLER_RUN_COMMAND} -- go run github.com/cosmtrek/air@v1.51.0 \
		-c .air.toml
		# --build.cmd "make build" --build.run "${BUILD_OUTPUT}" --build.delay "100" \
		# --build.exclude_dir "" \
		# --build.include_ext "go, tpl, tmpl, html, css, scss, js, ts, sql, jpeg, jpg, gif, png, bmp, svg, webp, ico" \
		# --misc.clean_on_exit "true"

# ==================================================================================== #
# OPERATIONS
# ==================================================================================== #

## push: push changes to the remote Git repository
.PHONY: push
push: tidy audit no-dirty
	git push

## production/deploy: deploy the application to production
.PHONY: production/deploy
production/deploy: tidy  # audit # (not going to audit right now)
	cd www && npm run build
	go build -o=${BUILD_OUTPUT} ${MAIN_PACKAGE_PATH}
	#upx -5 ${BUILD_OUTPUT}
	# include additional deployment steps here
