package handler

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"gitlab.com/sam_oneal/pawn_manager/internal/handler/dto"
	"gitlab.com/sam_oneal/pawn_manager/storage"
	storagedto "gitlab.com/sam_oneal/pawn_manager/storage/dto"
	"go.jetify.com/typeid"
	"log/slog"
	"maps"
	"net/http"
	"slices"
	"time"
)

func (a *Application) HandleGetCustomerPawns(c *gin.Context) {
	ctx := c.Request.Context()

	customerID := c.GetString("customer_id")

	if customerID == "" {
		slog.Warn("missing customer_id from context")
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "missing customer_id from context",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	log := slog.With("customer_id", customerID)

	log.Info("getting customer pawns")

	dbPawns, err := a.storage.GetPawns(ctx, customerID)
	if err != nil {
		log.Error("getting customer pawns from database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "error getting customer pawns from storage",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	pawnsMap := map[string]dto.Pawn{}
	for _, dbPawn := range dbPawns {
		// ensure pawn is in map
		pawn, ok := pawnsMap[dbPawn.Pawn.ID]
		if !ok {
			pawn = dto.Pawn{
				ID:          dbPawn.Pawn.ID,
				CustomerID:  dbPawn.Pawn.CustomerID,
				DateIn:      dbPawn.Pawn.DateIn,
				DateOut:     dbPawn.Pawn.DateOut,
				ForfeitDate: dbPawn.Pawn.ForfeitDate,
				Renewal:     int(dbPawn.Pawn.RenewalCount.Int32),
			}
			pawnsMap[dbPawn.Pawn.ID] = pawn
		}

		pawn.Items = append(pawn.Items, dto.PawnItem{
			ID:           dbPawn.PawnItem.ID,
			Description:  dbPawn.PawnItem.Description.String,
			Make:         dbPawn.PawnItem.Make,
			Model:        dbPawn.PawnItem.Model,
			SerialNumber: dbPawn.PawnItem.SerialNumber,
			Amount:       dbPawn.PawnItem.PawnAmount,
			Fee:          dbPawn.PawnItem.Fee,
			Total:        dbPawn.PawnItem.Total,
		})
	}

	pawns := slices.Collect(maps.Values(pawnsMap))

	c.JSON(http.StatusOK, dto.GetPawnsResponse{
		Data: pawns,
	})
}

func (a *Application) HandleGetCustomerPawn(c *gin.Context) {
	ctx := c.Request.Context()

	customerID := c.GetString("customer_id")
	if customerID == "" {
		slog.Warn("missing customer_id from context")
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "missing customer_id from context",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	log := slog.With("customer_id", customerID)

	pawnID := c.Param("pawn_id")
	if pawnID == "" {
		slog.Error("missing pawn_id from param")
		c.JSON(http.StatusBadRequest, dto.APIError{
			Message:   "missing pawn_id from param",
			Code:      http.StatusBadRequest,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}
	log = log.With("pawn_id", pawnID)

	log.Info("getting pawn from database")

	dbPawn, err := a.storage.GetPawn(ctx, storage.GetPawnParams{
		CustomerID: customerID,
		ID:         pawnID,
	})
	if err != nil {
		log.Error("getting pawn from database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "error getting pawn from database",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	if dbPawn == nil {
		log.Error("no pawn found")
		c.JSON(http.StatusNotFound, dto.APIError{
			Message:   "no pawn found",
			Code:      http.StatusNotFound,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	dbPawnItems, err := a.storage.GetPawnItemsByPawnID(ctx, dbPawn.ID)
	if err != nil {
		log.Error("getting pawn items from database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "error getting pawn items from database",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	pawn := dto.Pawn{
		ID:          dbPawn.ID,
		CustomerID:  dbPawn.CustomerID,
		DateIn:      dbPawn.DateIn,
		DateOut:     dbPawn.DateOut,
		ForfeitDate: dbPawn.ForfeitDate,
		Renewal:     int(dbPawn.RenewalCount.Int32),
	}

	for _, dbPawnItem := range dbPawnItems {
		pawn.Items = append(pawn.Items, dto.PawnItem{
			ID:           dbPawnItem.ID,
			Description:  dbPawnItem.Description.String,
			Make:         dbPawnItem.Make,
			Model:        dbPawnItem.Model,
			SerialNumber: dbPawnItem.SerialNumber,
			Amount:       dbPawnItem.PawnAmount,
			Fee:          dbPawnItem.Fee,
			Total:        dbPawnItem.Total,
		})
	}

	c.JSON(http.StatusOK, pawn)
}

func (a *Application) HandleCreatePawn(c *gin.Context) {
	ctx := c.Request.Context()

	customerID := c.GetString("customer_id")
	if customerID == "" {
		slog.Warn("missing customer_id from context")
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "missing customer_id from context",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	log := slog.With("customer_id", customerID)

	var request dto.CreatePawnRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		log.Error("binding request", "error", err)
		c.JSON(http.StatusBadRequest, dto.APIError{
			Message:   err.Error(),
			Code:      http.StatusBadRequest,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	pawnID, err := typeid.New[storagedto.PawnID]()
	if err != nil {
		log.Error("creating pawn id", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "error creating pawn id",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	dbRenewal := sql.NullInt32{
		Valid: true,
		Int32: int32(request.Renewal),
	}

	log.Info("creating pawn", "pawn_id", pawnID)

	if err := a.storage.CreatePawn(ctx, storage.CreatePawnParams{
		ID:           pawnID.String(),
		CustomerID:   customerID,
		DateIn:       request.DateIn,
		DateOut:      request.DateOut,
		ForfeitDate:  request.ForfeitDate,
		RenewalCount: dbRenewal,
	}); err != nil {
		log.Error("creating pawn in database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "error creating pawn",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	pawn := dto.Pawn{
		ID:          pawnID.String(),
		DateIn:      request.DateIn,
		DateOut:     request.DateOut,
		ForfeitDate: request.ForfeitDate,
		Renewal:     request.Renewal,
		Items:       make([]dto.PawnItem, len(request.Items)),
	}

	for _, reqPawnItem := range request.Items {
		pawnItemID, err := typeid.New[storagedto.PawnItemID]()
		if err != nil {
			log.Error("creating pawn item id", "error", err)
			c.JSON(http.StatusInternalServerError, dto.APIError{
				Message:   "error creating pawn item id",
				Code:      http.StatusInternalServerError,
				Timestamp: time.Now(),
				Path:      c.Request.URL.Path,
			})
			return
		}

		dbDescription := sql.NullString{
			Valid:  true,
			String: reqPawnItem.Description,
		}

		if err := a.storage.CreatePawnItem(ctx, storage.CreatePawnItemParams{
			ID:           pawnItemID.String(),
			PawnID:       pawnID.String(),
			Description:  dbDescription,
			Make:         reqPawnItem.Make,
			Model:        reqPawnItem.Model,
			SerialNumber: reqPawnItem.SerialNumber,
			PawnAmount:   reqPawnItem.Amount,
			Fee:          reqPawnItem.Fee,
			Total:        reqPawnItem.Total,
		}); err != nil {
			log.Error("creating pawn item in database", "error", err)
			c.JSON(http.StatusInternalServerError, dto.APIError{
				Message:   "error creating pawn item",
				Code:      http.StatusInternalServerError,
				Timestamp: time.Now(),
				Path:      c.Request.URL.Path,
			})
			return
		}

		pawn.Items = append(pawn.Items, dto.PawnItem{
			ID:           pawnItemID.String(),
			Description:  reqPawnItem.Description,
			Make:         reqPawnItem.Make,
			Model:        reqPawnItem.Model,
			SerialNumber: reqPawnItem.SerialNumber,
			Amount:       reqPawnItem.Amount,
			Fee:          reqPawnItem.Fee,
			Total:        reqPawnItem.Total,
		})
	}

	c.JSON(http.StatusCreated, pawn)
}
