package handler_test

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"github.com/stretchr/testify/assert"
	"io"
	"testing"
)

var key = make([]byte, 32)

func getTestSecureSSN(t *testing.T, ssn []byte) []byte {
	t.Helper()

	if key == nil {
		_, err := rand.Reader.Read(key)
		assert.NoError(t, err)
	}

	block, err := aes.NewCipher(key)
	assert.NoError(t, err)

	gcm, err := cipher.NewGCM(block)
	assert.NoError(t, err)

	nonce := make([]byte, gcm.NonceSize())
	_, err = io.ReadFull(rand.Reader, nonce)
	assert.NoError(t, err)

	return gcm.Seal(nonce, nonce, ssn, nil)
}

func TestHandleGetCustomers(t *testing.T) {
	t.Skip()
}
