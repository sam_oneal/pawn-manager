package handler

import (
	"crypto/rand"
	"github.com/gin-gonic/gin"
	"gitlab.com/sam_oneal/pawn_manager/internal/handler/dto"
	"gitlab.com/sam_oneal/pawn_manager/storage"
	storagedto "gitlab.com/sam_oneal/pawn_manager/storage/dto"
	"go.jetify.com/typeid"
	"golang.org/x/crypto/bcrypt"
	"log/slog"
	"net/http"
	"time"
)

// getBrokerID is a middleware to get broker id from the URL
func getBrokerID(c *gin.Context) {
	brokerId := c.Param("broker_id")
	c.Set("broker_id", brokerId)
	c.Next()
}

func (a *Application) HandleGetBrokers(c *gin.Context) {
	ctx := c.Request.Context()

	dbBrokers, err := a.storage.GetBrokers(ctx)
	if err != nil {
		slog.Error("getting brokers from database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Code:      http.StatusInternalServerError,
			Message:   "database error",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	response := dto.GetBrokersResponse{
		Data: make([]dto.Broker, 0, len(dbBrokers)),
	}

	for _, broker := range dbBrokers {
		response.Data = append(response.Data, dto.Broker{
			ID:      broker.ID,
			Name:    broker.Name,
			FeeRate: broker.FeeRate,
		})
	}

	c.JSON(http.StatusOK, response)
}

func (a *Application) HandleGetBroker(c *gin.Context) {
	ctx := c.Request.Context()

	id := c.Param("broker_id")
	if id == "" {
		slog.Warn("missing broker id in request")
		c.JSON(http.StatusBadRequest, dto.APIError{
			Code:      http.StatusBadRequest,
			Message:   "missing broker id",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	broker, err := a.storage.GetBroker(ctx, id)
	if err != nil {
		slog.Error("getting broker from database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Code:      http.StatusInternalServerError,
			Message:   "database error",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	if broker == nil {
		slog.Warn("broker not found", "id", id)
		c.JSON(http.StatusNotFound, dto.APIError{
			Code:      http.StatusNotFound,
			Message:   "broker not found",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	c.JSON(http.StatusOK, dto.Broker{
		ID:      broker.ID,
		Name:    broker.Name,
		FeeRate: broker.FeeRate,
	})
}

func (a *Application) HandleCreateBroker(c *gin.Context) {
	ctx := c.Request.Context()

	var request dto.CreateBrokerRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		slog.Error("binding request", "error", err)
		c.JSON(http.StatusBadRequest, dto.APIError{
			Code:      http.StatusBadRequest,
			Message:   "bad request",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	slog.Info("storing new broker", "broker", request.Name)

	id, err := typeid.New[storagedto.BrokerID]()
	if err != nil {
		slog.Error("creating broker id", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Code:      http.StatusInternalServerError,
			Message:   "object creation error",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	encryptionKey := make([]byte, 32)
	if _, err := rand.Reader.Read(encryptionKey); err != nil {
		slog.Error("generating encryption key", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Code:      http.StatusInternalServerError,
			Message:   "object creation error",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(request.Password), bcrypt.DefaultCost)
	if err != nil {
		slog.Error("generating password hash", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Code:      http.StatusInternalServerError,
			Message:   "object creation error",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	params := storage.CreateBrokerParams{
		ID:            id.String(),
		Name:          request.Name,
		FeeRate:       request.FeeRate,
		Email:         request.Email,
		EncryptionKey: encryptionKey,
		Password:      hashedPassword,
	}

	if err := a.storage.CreateBroker(ctx, params); err != nil {
		slog.Error("storing new broker", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Code:      http.StatusInternalServerError,
			Message:   "database error",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	c.JSON(http.StatusCreated, dto.Broker{
		ID:      id.String(),
		Name:    request.Name,
		FeeRate: request.FeeRate,
	})
}
