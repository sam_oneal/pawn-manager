package handler_test

import (
	"database/sql"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sam_oneal/pawn_manager/internal/handler"
	"gitlab.com/sam_oneal/pawn_manager/mocks"
	"gitlab.com/sam_oneal/pawn_manager/storage"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestHandleGetCustomerPawns(t *testing.T) {
	dateIn := time.Now()
	dateOut := dateIn.Add(24 * time.Hour)
	forfeitDate := dateOut.Add(48 * time.Hour)

	tests := []struct {
		name           string
		giveCustomerID string
		dbPawns        []*storage.GetPawnsRow
		dbErr          error
		setupDBMock    func(sqlmock *mocks.Storage, pawns []*storage.GetPawnsRow, dbErr error)
		wantStatusCode int
		wantBody       string
	}{
		{
			name:           "gets a list of pawns",
			giveCustomerID: "123",
			dbPawns: []*storage.GetPawnsRow{
				{
					Pawn: storage.Pawn{
						ID:           "123",
						CustomerID:   "123",
						DateIn:       dateIn,
						DateOut:      dateOut,
						ForfeitDate:  forfeitDate,
						RenewalCount: sql.NullInt32{Valid: true, Int32: 1},
					},
				},
			},
			setupDBMock: func(sqlmock *mocks.Storage, pawns []*storage.GetPawnsRow, dbErr error) {
				sqlmock.On("GetPawns", t.Context(), "123").Return(pawns, dbErr).Once()
			},
			wantStatusCode: 200,
			wantBody:       fmt.Sprintf(`{"data":[{"customer_id": "123", "date_in": "%s", "date_out": "%s", "forfeit_date": "%s", "renewal": 1, "id": "123", "items": null}]}`, dateIn.Format(time.RFC3339Nano), dateOut.Format(time.RFC3339Nano), forfeitDate.Format(time.RFC3339Nano)),
		},
		{
			name: "returns 500 if no customer id found in context",
			setupDBMock: func(sqlmock *mocks.Storage, pawns []*storage.GetPawnsRow, dbErr error) {

			},
			wantStatusCode: http.StatusInternalServerError,
		},
		{
			name:           "returns 500 if db error",
			giveCustomerID: "123",
			dbErr:          assert.AnError,
			setupDBMock: func(sqlmock *mocks.Storage, pawns []*storage.GetPawnsRow, dbErr error) {
				sqlmock.On("GetPawns", t.Context(), "123").Return(nil, dbErr).Once()
			},
			wantStatusCode: http.StatusInternalServerError,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			storageMock := mocks.NewStorage(t)
			tt.setupDBMock(storageMock, tt.dbPawns, tt.dbErr)

			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			c.Request = httptest.NewRequestWithContext(t.Context(), http.MethodGet, "/", nil)

			c.Set("customer_id", tt.giveCustomerID)

			app := handler.NewApplication(storageMock)

			app.HandleGetCustomerPawns(c)

			assert.Equal(t, tt.wantStatusCode, w.Code)
			if tt.wantBody != "" {
				assert.JSONEq(t, tt.wantBody, w.Body.String())
			}
		})
	}
}

func TestHandleGetCustomerPawn(t *testing.T) {
	dateIn := time.Now()
	dateOut := dateIn.Add(24 * time.Hour)
	forfeitDate := dateOut.Add(48 * time.Hour)

	tests := []struct {
		name           string
		giveCustomerID string
		givePawnID     string
		dbPawn         *storage.Pawn
		dbPawnItems    []*storage.PawnItem
		dbErr          error
		setupDBMock    func(sqlMock *mocks.Storage, dbPawn *storage.Pawn, dbPawnItems []*storage.PawnItem, dbErr error)
		wantStatusCode int
		wantBody       string
	}{
		{
			name:           "gets a pawn given a customer and pawn id value",
			giveCustomerID: "123",
			givePawnID:     "123",
			dbPawn: &storage.Pawn{
				ID:           "123",
				CustomerID:   "123",
				DateIn:       dateIn,
				DateOut:      dateOut,
				ForfeitDate:  forfeitDate,
				RenewalCount: sql.NullInt32{Valid: true, Int32: 1},
			},
			dbPawnItems: []*storage.PawnItem{
				{
					ID:           "123",
					Description:  sql.NullString{Valid: true, String: "aDescription"},
					Make:         "aMake",
					Model:        "aModel",
					SerialNumber: "aSerialNumber",
					PawnAmount:   decimal.NewFromInt(1),
					Fee:          decimal.NewFromInt(1),
					Total:        decimal.NewFromInt(1),
				},
			},
			setupDBMock: func(sqlMock *mocks.Storage, dbPawn *storage.Pawn, dbPawnItems []*storage.PawnItem, dbErr error) {
				sqlMock.On("GetPawn", t.Context(), storage.GetPawnParams{
					CustomerID: "123",
					ID:         "123",
				}).Return(dbPawn, dbErr).Once()

				sqlMock.On("GetPawnItemsByPawnID", t.Context(), "123").Return(dbPawnItems, dbErr).Once()
			},
			wantStatusCode: 200,
			//wantBody:       fmt.Sprintf(`{}`),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			storageMock := mocks.NewStorage(t)
			tt.setupDBMock(storageMock, tt.dbPawn, tt.dbPawnItems, tt.dbErr)

			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			c.Request = httptest.NewRequestWithContext(t.Context(), http.MethodGet, "/", nil)

			c.AddParam("pawn_id", tt.givePawnID)
			c.Set("customer_id", tt.giveCustomerID)

			app := handler.NewApplication(storageMock)

			app.HandleGetCustomerPawn(c)

			assert.Equal(t, tt.wantStatusCode, w.Code)
			if tt.wantBody != "" {
				assert.JSONEq(t, tt.wantBody, w.Body.String())
			}
		})
	}
}
