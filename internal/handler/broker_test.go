package handler_test

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/sam_oneal/pawn_manager/internal/handler"
	"gitlab.com/sam_oneal/pawn_manager/mocks"
	"gitlab.com/sam_oneal/pawn_manager/storage"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetBrokers(t *testing.T) {
	tests := []struct {
		name          string
		giveDBBrokers []*storage.Broker
		giveDBErr     error
		setupDBMock   func(*mocks.Storage, []*storage.Broker, error)
		wantStatus    int
		wantBody      string
	}{
		{
			name: "success",
			giveDBBrokers: []*storage.Broker{
				{
					ID:      "1234",
					Name:    "broker1",
					FeeRate: decimal.NewFromFloat(1.5),
				},
			},
			setupDBMock: func(m *mocks.Storage, b []*storage.Broker, err error) {
				m.On("GetBrokers", t.Context()).Return(b, err).Once()
			},
			wantStatus: http.StatusOK,
			wantBody:   `{"data": [{"id": "1234", "name": "broker1", "fee_rate": "1.5"}]}`,
		},
		{
			name:      "database error",
			giveDBErr: assert.AnError,
			setupDBMock: func(m *mocks.Storage, b []*storage.Broker, err error) {
				m.On("GetBrokers", t.Context()).Return(nil, err).Once()
			},
			wantStatus: http.StatusInternalServerError,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			storageMock := mocks.NewStorage(t)
			tt.setupDBMock(storageMock, tt.giveDBBrokers, tt.giveDBErr)

			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)

			c.Request = httptest.NewRequestWithContext(t.Context(), http.MethodGet, "/", nil)

			app := handler.NewApplication(storageMock)

			app.HandleGetBrokers(c)

			assert.Equal(t, tt.wantStatus, w.Code)
			if tt.wantBody != "" {
				assert.JSONEq(t, tt.wantBody, w.Body.String())
			}
		})
	}
}

func TestGetBroker(t *testing.T) {
	tests := []struct {
		name         string
		giveBrokerID string
		giveDBBroker *storage.Broker
		giveDBErr    error
		setupDBMock  func(*mocks.Storage, *storage.Broker, error)
		wantStatus   int
		wantBody     string
	}{
		{
			name:         "success",
			giveBrokerID: "1234",
			giveDBBroker: &storage.Broker{
				ID:      "1234",
				Name:    "broker1",
				FeeRate: decimal.NewFromFloat(1.5),
			},
			setupDBMock: func(m *mocks.Storage, b *storage.Broker, err error) {
				m.On("GetBroker", t.Context(), "1234").Return(b, err).Once()
			},
			wantStatus: http.StatusOK,
			wantBody:   `{"id": "1234", "name": "broker1", "fee_rate": "1.5"}`,
		},
		{
			name: "missing broker_id in request param",
			setupDBMock: func(m *mocks.Storage, b *storage.Broker, err error) {
			},
			wantStatus: http.StatusBadRequest,
		},
		{
			name:         "database error",
			giveBrokerID: "1234",
			giveDBErr:    assert.AnError,
			setupDBMock: func(m *mocks.Storage, b *storage.Broker, err error) {
				m.On("GetBroker", t.Context(), "1234").Return(nil, err).Once()
			},
			wantStatus: http.StatusInternalServerError,
		},
		{
			name:         "broker not found",
			giveBrokerID: "1234",
			setupDBMock: func(m *mocks.Storage, b *storage.Broker, err error) {
				m.On("GetBroker", t.Context(), "1234").Return(nil, nil).Once()
			},
			wantStatus: http.StatusNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			storageMock := mocks.NewStorage(t)
			tt.setupDBMock(storageMock, tt.giveDBBroker, tt.giveDBErr)

			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			c.Request = httptest.NewRequestWithContext(t.Context(), http.MethodGet, "/", nil)

			c.AddParam("broker_id", tt.giveBrokerID)

			app := handler.NewApplication(storageMock)

			app.HandleGetBroker(c)

			assert.Equal(t, tt.wantStatus, w.Code)
			if tt.wantBody != "" {
				assert.JSONEq(t, tt.wantBody, w.Body.String())
			}
		})
	}
}

func TestHandleCreateBroker(t *testing.T) {
	tests := []struct {
		name        string
		giveRequest map[string]any
		giveDBErr   error
		setupDBMock func(*mocks.Storage, error)
		wantStatus  int
	}{
		{
			name: "success",
			giveRequest: map[string]any{
				"name":     "broker1",
				"fee_rate": "1.5",
			},
			setupDBMock: func(m *mocks.Storage, err error) {
				m.On("CreateBroker", t.Context(), mock.Anything).Return(err).Once()
			},
			wantStatus: http.StatusCreated,
		},
		{
			name: "invalid request body",
			giveRequest: map[string]any{
				"test": "123",
			},
			setupDBMock: func(m *mocks.Storage, err error) {},
			wantStatus:  http.StatusBadRequest,
		},
		{
			name: "database error",
			giveRequest: map[string]any{
				"name":     "broker1",
				"fee_rate": "1.5",
			},
			giveDBErr: assert.AnError,
			setupDBMock: func(m *mocks.Storage, err error) {
				m.On("CreateBroker", t.Context(), mock.Anything).Return(err).Once()
			},
			wantStatus: http.StatusInternalServerError,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			storageMock := mocks.NewStorage(t)
			tt.setupDBMock(storageMock, tt.giveDBErr)

			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)

			reqBody, err := json.Marshal(tt.giveRequest)
			assert.NoError(t, err)

			c.Request = httptest.NewRequestWithContext(t.Context(), http.MethodPost, "/", bytes.NewReader(reqBody))

			app := handler.NewApplication(storageMock)

			app.HandleCreateBroker(c)

			assert.Equal(t, tt.wantStatus, w.Code)
		})
	}
}
