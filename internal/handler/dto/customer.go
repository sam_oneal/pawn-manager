package dto

import (
	"github.com/shopspring/decimal"
	"time"
)

type Customer struct {
	ID             string                 `json:"id"`
	FirstName      string                 `json:"first_name"`
	MiddleName     string                 `json:"middle_name"`
	LastName       string                 `json:"last_name"`
	Address        CustomerAddress        `json:"address"`
	Email          string                 `json:"email"`
	Phone          string                 `json:"phone"`
	DOB            time.Time              `json:"dob"`
	DriversLicense CustomerDriversLicense `json:"drivers_license"`
	SSN            string                 `json:"ssn"`
	Sex            string                 `json:"sex"`
	Race           string                 `json:"race"`
	Height         decimal.Decimal        `json:"height"`
	Weight         decimal.Decimal        `json:"weight"`
}

type CustomerDriversLicense struct {
	LicenseNumber string    `json:"license_number"`
	Expiration    time.Time `json:"expiration"`
}

type CustomerAddress struct {
	Line1      string `json:"line_1"`
	Line2      string `json:"line_2"`
	Line3      string `json:"line_3"`
	City       string `json:"city"`
	State      string `json:"state"`
	PostalCode string `json:"postal_code"`
}

type GetCustomersResponse struct {
	Data []Customer `json:"data"`
}
