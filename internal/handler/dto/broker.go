package dto

import "github.com/shopspring/decimal"

type Broker struct {
	ID      string          `json:"id"`
	Name    string          `json:"name"`
	FeeRate decimal.Decimal `json:"fee_rate"`
	Email   string          `json:"email"`
}

type GetBrokersResponse struct {
	Data []Broker `json:"data"`
}

type CreateBrokerRequest struct {
	Name     string          `json:"name" binding:"required"`
	FeeRate  decimal.Decimal `json:"fee_rate"  binding:"required"`
	Email    string          `json:"email" binding:"required"`
	Password string          `json:"password" binding:"required"`
}
