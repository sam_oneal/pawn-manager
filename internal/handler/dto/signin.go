package dto

type PostSignInRequest struct {
	Email    string `json:"email"  binding:"required"`
	Password string `json:"password"   binding:"required"`
}

type PostSignInResponse struct {
	Token   string `json:"token"`
	Expires int64  `json:"expires"`
}
