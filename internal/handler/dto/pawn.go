package dto

import (
	"github.com/shopspring/decimal"
	"time"
)

type Pawn struct {
	ID          string     `json:"id"`
	CustomerID  string     `json:"customer_id"`
	DateIn      time.Time  `json:"date_in"`
	DateOut     time.Time  `json:"date_out"`
	ForfeitDate time.Time  `json:"forfeit_date"`
	Renewal     int        `json:"renewal"`
	Items       []PawnItem `json:"items"`
}

type PawnItem struct {
	ID           string          `json:"id"`
	Description  string          `json:"description"`
	Make         string          `json:"make"`
	Model        string          `json:"model"`
	SerialNumber string          `json:"serial_number"`
	Amount       decimal.Decimal `json:"amount"`
	Fee          decimal.Decimal `json:"fee"`
	Total        decimal.Decimal `json:"total"`
}

type GetPawnsResponse struct {
	Data []Pawn `json:"data"`
}

type CreatePawnItemRequest struct {
	Description  string          `json:"description"`
	Make         string          `json:"make"`
	Model        string          `json:"model"`
	SerialNumber string          `json:"serial_number"`
	Amount       decimal.Decimal `json:"amount"`
	Fee          decimal.Decimal `json:"fee"`
	Total        decimal.Decimal `json:"total"`
}
type CreatePawnRequest struct {
	DateIn      time.Time               `json:"date_in"`
	DateOut     time.Time               `json:"date_out"`
	ForfeitDate time.Time               `json:"forfeit_date"`
	Renewal     int                     `json:"renewal"`
	Items       []CreatePawnItemRequest `json:"items"`
}

type UpdatePawnRequest struct {
	Pawn
}
