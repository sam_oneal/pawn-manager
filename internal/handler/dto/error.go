package dto

import "time"

type APIError struct {
	Message   string    `json:"message"`
	Code      int       `json:"code"`
	Timestamp time.Time `json:"timestamp"`
	Path      string    `json:"path"`
}
