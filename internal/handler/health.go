package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (a *Application) HandleGetHealth(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "ok"})
}
