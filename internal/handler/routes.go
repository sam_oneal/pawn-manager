package handler

import "github.com/gin-gonic/gin"

func (a *Application) GetRoutes(router *gin.Engine) {
	router.GET("/health", a.HandleGetHealth)

	api := router.Group("/api")

	v1 := api.Group("/v1")

	{
		v1.POST("/login", a.HandlePostSignIn)

		brokers := v1.Group("/brokers")

		brokers.GET("", a.HandleGetBrokers)
		brokers.POST("", a.HandleCreateBroker)
		broker := brokers.Group("/:broker_id")
		{
			broker.Use(getBrokerID)
			broker.GET("", a.HandleGetBroker)
			customers := broker.Group("/customers")
			{
				customers.GET("", a.HandleGetCustomers)
				customer := customers.Group("/:customer_id")
				{
					customer.Use(getCustomer)
					customer.GET("", a.HandleGetCustomer)
					pawns := customer.Group("/pawns")
					{
						pawns.GET("", a.HandleGetCustomerPawns)
						pawns.GET("/:pawn_id", a.HandleGetCustomerPawn)
						pawns.POST("", a.HandleCreatePawn)
					}
				}
			}
			//pawns := broker.Group("/pawns")
			//{
			//	pawns.GET("", a.HandleGetPawns)
			//	pawns.GET("/:pawn_id", a.HandleGetPawn)
			//}
		}
	}
}
