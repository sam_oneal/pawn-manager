package handler

import (
	"context"
	"gitlab.com/sam_oneal/pawn_manager/storage"
)

type Storage interface {
	GetPawns(context.Context, string) ([]*storage.GetPawnsRow, error)
	GetPawn(context.Context, storage.GetPawnParams) (*storage.Pawn, error)
	GetPawnItemsByPawnID(context.Context, string) ([]*storage.PawnItem, error)
	CreatePawn(context.Context, storage.CreatePawnParams) error
	CreatePawnItem(context.Context, storage.CreatePawnItemParams) error
	GetCustomers(context.Context) ([]*storage.Customer, error)
	GetCustomer(context.Context, string) (*storage.Customer, error)
	CreateCustomer(context.Context, storage.CreateCustomerParams) error
	GetBrokers(context.Context) ([]*storage.Broker, error)
	GetBroker(context.Context, string) (*storage.Broker, error)
	GetBrokerByEmail(context.Context, string) (*storage.Broker, error)
	CreateBroker(context.Context, storage.CreateBrokerParams) error
}
type Application struct {
	storage       Storage
	encryptionKey []byte
}

func NewApplication(storage Storage, encryptionKey []byte) *Application {
	return &Application{
		storage:       storage,
		encryptionKey: encryptionKey,
	}
}
