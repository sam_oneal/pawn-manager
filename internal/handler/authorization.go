package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/sam_oneal/pawn_manager/internal/handler/dto"
	"log/slog"
	"net/http"
	"strings"
	"time"
)

type BrokerClaims struct {
	BrokerID string `json:"broker_id"`
	Email    string `json:"email"`
	jwt.RegisteredClaims
}

func (a *Application) HandleAuthorization(c *gin.Context) {
	auth := c.GetHeader("Authorization")

	if auth == "" {
		slog.Warn("attempting to access without authorization header")
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	authSlice := strings.Split(auth, " ")
	if len(authSlice) != 2 {
		slog.Warn("invalid authorization header")
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	if authSlice[0] != "Bearer" {
		slog.Warn(fmt.Sprintf("authorization header %s is not supproted", authSlice[0]))
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	claims := BrokerClaims{}

	token, err := jwt.ParseWithClaims(authSlice[1], &claims, func(token *jwt.Token) (any, error) {
		return a.encryptionKey, nil
	})

	if err != nil {
		slog.Warn("parsing jwt token", "error", err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, dto.APIError{
			Code:      http.StatusInternalServerError,
			Message:   "parsing jwt token failed",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	if !token.Valid {
		slog.Warn("invalid jwt token")
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	c.Set("broker_id", claims.BrokerID)
	c.Set("broker_email", claims.Email)

	c.Next()
}
