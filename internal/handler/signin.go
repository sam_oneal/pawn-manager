package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/sam_oneal/pawn_manager/internal/handler/dto"
	"golang.org/x/crypto/bcrypt"
	"log/slog"
	"net/http"
	"time"
)

func (a *Application) HandlePostSignIn(c *gin.Context) {
	ctx := c.Request.Context()

	var request dto.PostSignInRequest

	if err := c.ShouldBindJSON(&request); err != nil {
		slog.Error("binding request", "error", err)
		c.JSON(http.StatusBadRequest, dto.APIError{
			Code:      http.StatusBadRequest,
			Message:   "invalid request",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	slog.Info("checking broker exist in the system", "email", request.Email)

	broker, err := a.storage.GetBrokerByEmail(ctx, request.Email)
	if err != nil {
		slog.Error("getting broker from database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Code:      http.StatusInternalServerError,
			Message:   "database error",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	if broker == nil {
		slog.Warn("broker not found", "error", err)
		c.JSON(http.StatusBadRequest, dto.APIError{
			Code:      http.StatusBadRequest,
			Message:   "invalid email/password",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	slog.Debug("checking password")

	if err := bcrypt.CompareHashAndPassword(broker.Password, []byte(request.Password)); err != nil {
		slog.Error("comparing password", "error", err)
		c.JSON(http.StatusBadRequest, dto.APIError{
			Code:      http.StatusBadRequest,
			Message:   "invalid email/password",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	// generate jwt
	token := jwt.NewWithClaims(jwt.SigningMethodHS512,
		jwt.MapClaims{
			"iss":      "pawn-manager",
			"sub":      broker.Email,
			"brokerId": broker.ID,
			"exp":      time.Now().Add(time.Hour).Unix(),
		})
	signedToken, err := token.SignedString(a.encryptionKey)
	if err != nil {
		slog.Error("signing token", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Code:      http.StatusInternalServerError,
			Message:   "token creation error",
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
	}

	response := dto.PostSignInResponse{
		Token:   signedToken,
		Expires: token.Claims.(jwt.MapClaims)["exp"].(int64),
	}

	c.JSON(http.StatusOK, response)
}
