package handler

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/sam_oneal/pawn_manager/internal/handler/dto"
	"gitlab.com/sam_oneal/pawn_manager/storage"
	"io"
	"log/slog"
	"net/http"
	"time"
)

// getCustomer is a middleware to pull the customer id from the URL
func getCustomer(c *gin.Context) {
	// /customers/:customer_id
	customerId := c.Param("customer_id")

	c.Set("customer_id", customerId)

	c.Next()
}

func (a *Application) HandleGetCustomers(c *gin.Context) {
	ctx := c.Request.Context()

	brokerID := c.GetString("broker_id")
	if brokerID == "" {
		slog.Warn("broker not specified")
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "broker not specified",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	broker, err := a.storage.GetBroker(ctx, brokerID)
	if err != nil {
		slog.Error("getting broker from database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "database error",
			Code:      http.StatusInternalServerError,
			Path:      c.Request.URL.Path,
			Timestamp: time.Now(),
		})
		return
	}

	dbCustomers, err := a.storage.GetCustomers(ctx)
	if err != nil {
		slog.Error("reading customer from db", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "database error",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	response := dto.GetCustomersResponse{
		Data: make([]dto.Customer, 0, len(dbCustomers)),
	}

	for _, dbCustomer := range dbCustomers {
		customer, err := transformCustomer(dbCustomer, broker.EncryptionKey)

		if err != nil {
			slog.Error("transforming customer", "error", err)
			c.JSON(http.StatusInternalServerError, dto.APIError{
				Message:   "parsing  error",
				Code:      http.StatusInternalServerError,
				Timestamp: time.Now(),
				Path:      c.Request.URL.Path,
			})
			return
		}

		response.Data = append(response.Data, customer)
	}

	c.JSON(http.StatusOK, response)
}

func (a *Application) HandleGetCustomer(c *gin.Context) {
	ctx := c.Request.Context()

	brokerID := c.GetString("broker_id")
	if brokerID == "" {
		slog.Warn("broker not specified")
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "broker not specified",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	broker, err := a.storage.GetBroker(ctx, brokerID)
	if err != nil {
		slog.Error("getting broker from database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "database error",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	customerID := c.Param("customer_id")
	if customerID == "" {
		slog.Warn("customer not specified")
		c.JSON(http.StatusBadRequest, dto.APIError{
			Message:   "customer not specified",
			Code:      http.StatusBadRequest,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	dbCustomer, err := a.storage.GetCustomer(ctx, customerID)
	if err != nil {
		slog.Error("getting customer from database", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "database error",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	if dbCustomer == nil {
		slog.Warn("customer not found")
		c.JSON(http.StatusNotFound, dto.APIError{
			Message:   "customer not found",
			Code:      http.StatusNotFound,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	customer, err := transformCustomer(dbCustomer, broker.EncryptionKey)
	if err != nil {
		slog.Error("transforming customer", "error", err)
		c.JSON(http.StatusInternalServerError, dto.APIError{
			Message:   "parsing  error",
			Code:      http.StatusInternalServerError,
			Timestamp: time.Now(),
			Path:      c.Request.URL.Path,
		})
		return
	}

	c.JSON(http.StatusOK, customer)
}

func transformCustomer(dbCustomer *storage.Customer, encryptionKey []byte) (dto.Customer, error) {
	customer := dto.Customer{
		ID:         dbCustomer.ID,
		FirstName:  dbCustomer.FirstName,
		MiddleName: dbCustomer.MiddleName.String,
		LastName:   dbCustomer.LastName,
		Email:      dbCustomer.Email,
		DOB:        dbCustomer.Dob,
		Phone:      dbCustomer.Phone,
		DriversLicense: dto.CustomerDriversLicense{
			LicenseNumber: dbCustomer.DriversLicense,
			Expiration:    dbCustomer.DriversLicenseExpDate,
		},
		Sex:    dbCustomer.Sex.String,
		Race:   dbCustomer.Race.String,
		Height: dbCustomer.Height,
		Weight: dbCustomer.Weight,
	}

	if ssn, err := decryptSSN(dbCustomer.Ssn, encryptionKey); err != nil {
		return customer, fmt.Errorf("decrypting SSN: %w", err)
	} else {
		customer.SSN = ssn
	}

	addrByte, err := dbCustomer.Address.MarshalJSON()
	if err != nil {
		return customer, fmt.Errorf("marshalling address from database return: %w", err)
	}

	if err := json.Unmarshal(addrByte, &customer.Address); err != nil {
		return customer, fmt.Errorf("unmarshalling address into api response: %w", err)
	}

	return customer, nil
}

func decryptSSN(encryptedSSN []byte, encryptKey []byte) (string, error) {
	block, err := aes.NewCipher(encryptKey)
	if err != nil {
		return "", fmt.Errorf("creating cipher block: %w", err)
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", fmt.Errorf("setting gcm mode: %w", err)
	}

	data, err := aesgcm.Open(nil, encryptedSSN[:aesgcm.NonceSize()], encryptedSSN[aesgcm.NonceSize():], nil)
	if err != nil {
		return "", fmt.Errorf("decyrpting data: %w", err)
	}

	return string(data), nil
}

func encryptSSN(ssn string, encryptKey []byte) ([]byte, error) {
	block, err := aes.NewCipher(encryptKey)
	if err != nil {
		return nil, fmt.Errorf("creating cipher block: %w", err)
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, fmt.Errorf("setting gcm mode: %w", err)
	}

	nonce := make([]byte, aesgcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, fmt.Errorf("generating nonce: %w", err)
	}

	return aesgcm.Seal(nonce, nonce, []byte(ssn), nil), nil
}
