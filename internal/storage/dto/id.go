package dto

import "go.jetify.com/typeid"

type PawnPrefix struct{}

func (PawnPrefix) Prefix() string { return "pawn" }

type PawnID struct {
	typeid.TypeID[PawnPrefix]
}

type PawnItemPrefix struct{}

func (PawnItemPrefix) Prefix() string { return "item" }

type PawnItemID struct {
	typeid.TypeID[PawnItemPrefix]
}

type BrokerPrefix struct{}

func (BrokerPrefix) Prefix() string { return "broker" }

type BrokerID struct {
	typeid.TypeID[BrokerPrefix]
}
