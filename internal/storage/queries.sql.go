// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.28.0
// source: queries.sql

package storage

import (
	"context"
	"database/sql"
	"encoding/json"
	"time"

	"github.com/shopspring/decimal"
)

const createBroker = `-- name: CreateBroker :exec
INSERT INTO broker (id, name, fee_rate, email, password, encryption_key) VALUES ($1, $2, $3, $4, $5, $6)
`

type CreateBrokerParams struct {
	ID            string
	Name          string
	FeeRate       decimal.Decimal
	Email         string
	Password      []byte
	EncryptionKey []byte
}

func (q *Queries) CreateBroker(ctx context.Context, arg CreateBrokerParams) error {
	_, err := q.db.ExecContext(ctx, createBroker,
		arg.ID,
		arg.Name,
		arg.FeeRate,
		arg.Email,
		arg.Password,
		arg.EncryptionKey,
	)
	return err
}

const createCustomer = `-- name: CreateCustomer :exec
INSERT INTO customer (id, first_name, middle_name, last_name, address, phone, dob, drivers_license, drivers_license_exp_date, ssn, sex, race, height, weight)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
`

type CreateCustomerParams struct {
	ID                    string
	FirstName             string
	MiddleName            sql.NullString
	LastName              string
	Address               json.RawMessage
	Phone                 string
	Dob                   time.Time
	DriversLicense        string
	DriversLicenseExpDate time.Time
	Ssn                   []byte
	Sex                   sql.NullString
	Race                  sql.NullString
	Height                decimal.Decimal
	Weight                decimal.Decimal
}

func (q *Queries) CreateCustomer(ctx context.Context, arg CreateCustomerParams) error {
	_, err := q.db.ExecContext(ctx, createCustomer,
		arg.ID,
		arg.FirstName,
		arg.MiddleName,
		arg.LastName,
		arg.Address,
		arg.Phone,
		arg.Dob,
		arg.DriversLicense,
		arg.DriversLicenseExpDate,
		arg.Ssn,
		arg.Sex,
		arg.Race,
		arg.Height,
		arg.Weight,
	)
	return err
}

const createPawn = `-- name: CreatePawn :exec
INSERT INTO pawn (id, customer_id, date_in, date_out, forfeit_date, renewal_count)
    VALUES ($1, $2, $3, $4, $5, $6)
`

type CreatePawnParams struct {
	ID           string
	CustomerID   string
	DateIn       time.Time
	DateOut      time.Time
	ForfeitDate  time.Time
	RenewalCount sql.NullInt32
}

func (q *Queries) CreatePawn(ctx context.Context, arg CreatePawnParams) error {
	_, err := q.db.ExecContext(ctx, createPawn,
		arg.ID,
		arg.CustomerID,
		arg.DateIn,
		arg.DateOut,
		arg.ForfeitDate,
		arg.RenewalCount,
	)
	return err
}

const createPawnItem = `-- name: CreatePawnItem :exec
INSERT INTO pawn_item (id, pawn_id, description, make, model, serial_number, pawn_amount, fee, total)
    VALUES ($1, $2, $3, $4, $5, $6,  $7, $8, $9)
`

type CreatePawnItemParams struct {
	ID           string
	PawnID       string
	Description  sql.NullString
	Make         string
	Model        string
	SerialNumber string
	PawnAmount   decimal.Decimal
	Fee          decimal.Decimal
	Total        decimal.Decimal
}

func (q *Queries) CreatePawnItem(ctx context.Context, arg CreatePawnItemParams) error {
	_, err := q.db.ExecContext(ctx, createPawnItem,
		arg.ID,
		arg.PawnID,
		arg.Description,
		arg.Make,
		arg.Model,
		arg.SerialNumber,
		arg.PawnAmount,
		arg.Fee,
		arg.Total,
	)
	return err
}

const getBroker = `-- name: GetBroker :one
SELECT id, name, encryption_key, fee_rate, email, password, created_at, updated_at, deleted_at FROM broker WHERE id = $1
`

func (q *Queries) GetBroker(ctx context.Context, id string) (*Broker, error) {
	row := q.db.QueryRowContext(ctx, getBroker, id)
	var i Broker
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.EncryptionKey,
		&i.FeeRate,
		&i.Email,
		&i.Password,
		&i.CreatedAt,
		&i.UpdatedAt,
		&i.DeletedAt,
	)
	return &i, err
}

const getBrokerByEmail = `-- name: GetBrokerByEmail :one
SELECT id, name, encryption_key, fee_rate, email, password, created_at, updated_at, deleted_at FROM broker WHERE email = $1
`

func (q *Queries) GetBrokerByEmail(ctx context.Context, email string) (*Broker, error) {
	row := q.db.QueryRowContext(ctx, getBrokerByEmail, email)
	var i Broker
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.EncryptionKey,
		&i.FeeRate,
		&i.Email,
		&i.Password,
		&i.CreatedAt,
		&i.UpdatedAt,
		&i.DeletedAt,
	)
	return &i, err
}

const getBrokers = `-- name: GetBrokers :many
SELECT id, name, encryption_key, fee_rate, email, password, created_at, updated_at, deleted_at FROM broker
`

func (q *Queries) GetBrokers(ctx context.Context) ([]*Broker, error) {
	rows, err := q.db.QueryContext(ctx, getBrokers)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []*Broker
	for rows.Next() {
		var i Broker
		if err := rows.Scan(
			&i.ID,
			&i.Name,
			&i.EncryptionKey,
			&i.FeeRate,
			&i.Email,
			&i.Password,
			&i.CreatedAt,
			&i.UpdatedAt,
			&i.DeletedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const getCustomer = `-- name: GetCustomer :one
SELECT id, first_name, middle_name, last_name, address, phone, email, dob, drivers_license, drivers_license_exp_date, ssn, sex, race, height, weight, created_at, updated_at FROM customer WHERE id = $1
`

func (q *Queries) GetCustomer(ctx context.Context, id string) (*Customer, error) {
	row := q.db.QueryRowContext(ctx, getCustomer, id)
	var i Customer
	err := row.Scan(
		&i.ID,
		&i.FirstName,
		&i.MiddleName,
		&i.LastName,
		&i.Address,
		&i.Phone,
		&i.Email,
		&i.Dob,
		&i.DriversLicense,
		&i.DriversLicenseExpDate,
		&i.Ssn,
		&i.Sex,
		&i.Race,
		&i.Height,
		&i.Weight,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return &i, err
}

const getCustomers = `-- name: GetCustomers :many
SELECT id, first_name, middle_name, last_name, address, phone, email, dob, drivers_license, drivers_license_exp_date, ssn, sex, race, height, weight, created_at, updated_at FROM customer
`

func (q *Queries) GetCustomers(ctx context.Context) ([]*Customer, error) {
	rows, err := q.db.QueryContext(ctx, getCustomers)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []*Customer
	for rows.Next() {
		var i Customer
		if err := rows.Scan(
			&i.ID,
			&i.FirstName,
			&i.MiddleName,
			&i.LastName,
			&i.Address,
			&i.Phone,
			&i.Email,
			&i.Dob,
			&i.DriversLicense,
			&i.DriversLicenseExpDate,
			&i.Ssn,
			&i.Sex,
			&i.Race,
			&i.Height,
			&i.Weight,
			&i.CreatedAt,
			&i.UpdatedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const getPawn = `-- name: GetPawn :one
SELECT id, customer_id, date_in, date_out, forfeit_date, renewal_count, created_at, updated_at
    FROM pawn
    WHERE customer_id = $1 AND id = $2
`

type GetPawnParams struct {
	CustomerID string
	ID         string
}

func (q *Queries) GetPawn(ctx context.Context, arg GetPawnParams) (*Pawn, error) {
	row := q.db.QueryRowContext(ctx, getPawn, arg.CustomerID, arg.ID)
	var i Pawn
	err := row.Scan(
		&i.ID,
		&i.CustomerID,
		&i.DateIn,
		&i.DateOut,
		&i.ForfeitDate,
		&i.RenewalCount,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return &i, err
}

const getPawnItemsByPawnID = `-- name: GetPawnItemsByPawnID :many
SELECT id, pawn_id, description, make, model, serial_number, pawn_amount, fee, total, created_at, updated_at FROM pawn_item where pawn_id = $1
`

func (q *Queries) GetPawnItemsByPawnID(ctx context.Context, pawnID string) ([]*PawnItem, error) {
	rows, err := q.db.QueryContext(ctx, getPawnItemsByPawnID, pawnID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []*PawnItem
	for rows.Next() {
		var i PawnItem
		if err := rows.Scan(
			&i.ID,
			&i.PawnID,
			&i.Description,
			&i.Make,
			&i.Model,
			&i.SerialNumber,
			&i.PawnAmount,
			&i.Fee,
			&i.Total,
			&i.CreatedAt,
			&i.UpdatedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const getPawns = `-- name: GetPawns :many
SELECT pawn.id, pawn.customer_id, pawn.date_in, pawn.date_out, pawn.forfeit_date, pawn.renewal_count, pawn.created_at, pawn.updated_at, pawn_item.id, pawn_item.pawn_id, pawn_item.description, pawn_item.make, pawn_item.model, pawn_item.serial_number, pawn_item.pawn_amount, pawn_item.fee, pawn_item.total, pawn_item.created_at, pawn_item.updated_at
    FROM pawn
        LEFT JOIN pawn_item ON pawn_item.pawn_id = pawn.id
    WHERE customer_id = $1
`

type GetPawnsRow struct {
	Pawn     Pawn
	PawnItem PawnItem
}

func (q *Queries) GetPawns(ctx context.Context, customerID string) ([]*GetPawnsRow, error) {
	rows, err := q.db.QueryContext(ctx, getPawns, customerID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []*GetPawnsRow
	for rows.Next() {
		var i GetPawnsRow
		if err := rows.Scan(
			&i.Pawn.ID,
			&i.Pawn.CustomerID,
			&i.Pawn.DateIn,
			&i.Pawn.DateOut,
			&i.Pawn.ForfeitDate,
			&i.Pawn.RenewalCount,
			&i.Pawn.CreatedAt,
			&i.Pawn.UpdatedAt,
			&i.PawnItem.ID,
			&i.PawnItem.PawnID,
			&i.PawnItem.Description,
			&i.PawnItem.Make,
			&i.PawnItem.Model,
			&i.PawnItem.SerialNumber,
			&i.PawnItem.PawnAmount,
			&i.PawnItem.Fee,
			&i.PawnItem.Total,
			&i.PawnItem.CreatedAt,
			&i.PawnItem.UpdatedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
