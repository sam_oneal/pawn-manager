// Code generated by mockery v2.53.0. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	storage "gitlab.com/sam_oneal/pawn_manager/storage"
)

// Storage is an autogenerated mock type for the Storage type
type Storage struct {
	mock.Mock
}

type Storage_Expecter struct {
	mock *mock.Mock
}

func (_m *Storage) EXPECT() *Storage_Expecter {
	return &Storage_Expecter{mock: &_m.Mock}
}

// CreateBroker provides a mock function with given fields: _a0, _a1
func (_m *Storage) CreateBroker(_a0 context.Context, _a1 storage.CreateBrokerParams) error {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for CreateBroker")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, storage.CreateBrokerParams) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Storage_CreateBroker_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'CreateBroker'
type Storage_CreateBroker_Call struct {
	*mock.Call
}

// CreateBroker is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 storage.CreateBrokerParams
func (_e *Storage_Expecter) CreateBroker(_a0 interface{}, _a1 interface{}) *Storage_CreateBroker_Call {
	return &Storage_CreateBroker_Call{Call: _e.mock.On("CreateBroker", _a0, _a1)}
}

func (_c *Storage_CreateBroker_Call) Run(run func(_a0 context.Context, _a1 storage.CreateBrokerParams)) *Storage_CreateBroker_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(storage.CreateBrokerParams))
	})
	return _c
}

func (_c *Storage_CreateBroker_Call) Return(_a0 error) *Storage_CreateBroker_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *Storage_CreateBroker_Call) RunAndReturn(run func(context.Context, storage.CreateBrokerParams) error) *Storage_CreateBroker_Call {
	_c.Call.Return(run)
	return _c
}

// CreateCustomer provides a mock function with given fields: _a0, _a1
func (_m *Storage) CreateCustomer(_a0 context.Context, _a1 storage.CreateCustomerParams) error {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for CreateCustomer")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, storage.CreateCustomerParams) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Storage_CreateCustomer_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'CreateCustomer'
type Storage_CreateCustomer_Call struct {
	*mock.Call
}

// CreateCustomer is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 storage.CreateCustomerParams
func (_e *Storage_Expecter) CreateCustomer(_a0 interface{}, _a1 interface{}) *Storage_CreateCustomer_Call {
	return &Storage_CreateCustomer_Call{Call: _e.mock.On("CreateCustomer", _a0, _a1)}
}

func (_c *Storage_CreateCustomer_Call) Run(run func(_a0 context.Context, _a1 storage.CreateCustomerParams)) *Storage_CreateCustomer_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(storage.CreateCustomerParams))
	})
	return _c
}

func (_c *Storage_CreateCustomer_Call) Return(_a0 error) *Storage_CreateCustomer_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *Storage_CreateCustomer_Call) RunAndReturn(run func(context.Context, storage.CreateCustomerParams) error) *Storage_CreateCustomer_Call {
	_c.Call.Return(run)
	return _c
}

// CreatePawn provides a mock function with given fields: _a0, _a1
func (_m *Storage) CreatePawn(_a0 context.Context, _a1 storage.CreatePawnParams) error {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for CreatePawn")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, storage.CreatePawnParams) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Storage_CreatePawn_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'CreatePawn'
type Storage_CreatePawn_Call struct {
	*mock.Call
}

// CreatePawn is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 storage.CreatePawnParams
func (_e *Storage_Expecter) CreatePawn(_a0 interface{}, _a1 interface{}) *Storage_CreatePawn_Call {
	return &Storage_CreatePawn_Call{Call: _e.mock.On("CreatePawn", _a0, _a1)}
}

func (_c *Storage_CreatePawn_Call) Run(run func(_a0 context.Context, _a1 storage.CreatePawnParams)) *Storage_CreatePawn_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(storage.CreatePawnParams))
	})
	return _c
}

func (_c *Storage_CreatePawn_Call) Return(_a0 error) *Storage_CreatePawn_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *Storage_CreatePawn_Call) RunAndReturn(run func(context.Context, storage.CreatePawnParams) error) *Storage_CreatePawn_Call {
	_c.Call.Return(run)
	return _c
}

// CreatePawnItem provides a mock function with given fields: _a0, _a1
func (_m *Storage) CreatePawnItem(_a0 context.Context, _a1 storage.CreatePawnItemParams) error {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for CreatePawnItem")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, storage.CreatePawnItemParams) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Storage_CreatePawnItem_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'CreatePawnItem'
type Storage_CreatePawnItem_Call struct {
	*mock.Call
}

// CreatePawnItem is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 storage.CreatePawnItemParams
func (_e *Storage_Expecter) CreatePawnItem(_a0 interface{}, _a1 interface{}) *Storage_CreatePawnItem_Call {
	return &Storage_CreatePawnItem_Call{Call: _e.mock.On("CreatePawnItem", _a0, _a1)}
}

func (_c *Storage_CreatePawnItem_Call) Run(run func(_a0 context.Context, _a1 storage.CreatePawnItemParams)) *Storage_CreatePawnItem_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(storage.CreatePawnItemParams))
	})
	return _c
}

func (_c *Storage_CreatePawnItem_Call) Return(_a0 error) *Storage_CreatePawnItem_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *Storage_CreatePawnItem_Call) RunAndReturn(run func(context.Context, storage.CreatePawnItemParams) error) *Storage_CreatePawnItem_Call {
	_c.Call.Return(run)
	return _c
}

// GetBroker provides a mock function with given fields: _a0, _a1
func (_m *Storage) GetBroker(_a0 context.Context, _a1 string) (*storage.Broker, error) {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for GetBroker")
	}

	var r0 *storage.Broker
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) (*storage.Broker, error)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) *storage.Broker); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*storage.Broker)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Storage_GetBroker_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetBroker'
type Storage_GetBroker_Call struct {
	*mock.Call
}

// GetBroker is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 string
func (_e *Storage_Expecter) GetBroker(_a0 interface{}, _a1 interface{}) *Storage_GetBroker_Call {
	return &Storage_GetBroker_Call{Call: _e.mock.On("GetBroker", _a0, _a1)}
}

func (_c *Storage_GetBroker_Call) Run(run func(_a0 context.Context, _a1 string)) *Storage_GetBroker_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *Storage_GetBroker_Call) Return(_a0 *storage.Broker, _a1 error) *Storage_GetBroker_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Storage_GetBroker_Call) RunAndReturn(run func(context.Context, string) (*storage.Broker, error)) *Storage_GetBroker_Call {
	_c.Call.Return(run)
	return _c
}

// GetBrokerByEmail provides a mock function with given fields: _a0, _a1
func (_m *Storage) GetBrokerByEmail(_a0 context.Context, _a1 string) (*storage.Broker, error) {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for GetBrokerByEmail")
	}

	var r0 *storage.Broker
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) (*storage.Broker, error)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) *storage.Broker); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*storage.Broker)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Storage_GetBrokerByEmail_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetBrokerByEmail'
type Storage_GetBrokerByEmail_Call struct {
	*mock.Call
}

// GetBrokerByEmail is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 string
func (_e *Storage_Expecter) GetBrokerByEmail(_a0 interface{}, _a1 interface{}) *Storage_GetBrokerByEmail_Call {
	return &Storage_GetBrokerByEmail_Call{Call: _e.mock.On("GetBrokerByEmail", _a0, _a1)}
}

func (_c *Storage_GetBrokerByEmail_Call) Run(run func(_a0 context.Context, _a1 string)) *Storage_GetBrokerByEmail_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *Storage_GetBrokerByEmail_Call) Return(_a0 *storage.Broker, _a1 error) *Storage_GetBrokerByEmail_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Storage_GetBrokerByEmail_Call) RunAndReturn(run func(context.Context, string) (*storage.Broker, error)) *Storage_GetBrokerByEmail_Call {
	_c.Call.Return(run)
	return _c
}

// GetBrokers provides a mock function with given fields: _a0
func (_m *Storage) GetBrokers(_a0 context.Context) ([]*storage.Broker, error) {
	ret := _m.Called(_a0)

	if len(ret) == 0 {
		panic("no return value specified for GetBrokers")
	}

	var r0 []*storage.Broker
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context) ([]*storage.Broker, error)); ok {
		return rf(_a0)
	}
	if rf, ok := ret.Get(0).(func(context.Context) []*storage.Broker); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*storage.Broker)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Storage_GetBrokers_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetBrokers'
type Storage_GetBrokers_Call struct {
	*mock.Call
}

// GetBrokers is a helper method to define mock.On call
//   - _a0 context.Context
func (_e *Storage_Expecter) GetBrokers(_a0 interface{}) *Storage_GetBrokers_Call {
	return &Storage_GetBrokers_Call{Call: _e.mock.On("GetBrokers", _a0)}
}

func (_c *Storage_GetBrokers_Call) Run(run func(_a0 context.Context)) *Storage_GetBrokers_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context))
	})
	return _c
}

func (_c *Storage_GetBrokers_Call) Return(_a0 []*storage.Broker, _a1 error) *Storage_GetBrokers_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Storage_GetBrokers_Call) RunAndReturn(run func(context.Context) ([]*storage.Broker, error)) *Storage_GetBrokers_Call {
	_c.Call.Return(run)
	return _c
}

// GetCustomer provides a mock function with given fields: _a0, _a1
func (_m *Storage) GetCustomer(_a0 context.Context, _a1 string) (*storage.Customer, error) {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for GetCustomer")
	}

	var r0 *storage.Customer
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) (*storage.Customer, error)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) *storage.Customer); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*storage.Customer)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Storage_GetCustomer_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetCustomer'
type Storage_GetCustomer_Call struct {
	*mock.Call
}

// GetCustomer is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 string
func (_e *Storage_Expecter) GetCustomer(_a0 interface{}, _a1 interface{}) *Storage_GetCustomer_Call {
	return &Storage_GetCustomer_Call{Call: _e.mock.On("GetCustomer", _a0, _a1)}
}

func (_c *Storage_GetCustomer_Call) Run(run func(_a0 context.Context, _a1 string)) *Storage_GetCustomer_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *Storage_GetCustomer_Call) Return(_a0 *storage.Customer, _a1 error) *Storage_GetCustomer_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Storage_GetCustomer_Call) RunAndReturn(run func(context.Context, string) (*storage.Customer, error)) *Storage_GetCustomer_Call {
	_c.Call.Return(run)
	return _c
}

// GetCustomers provides a mock function with given fields: _a0
func (_m *Storage) GetCustomers(_a0 context.Context) ([]*storage.Customer, error) {
	ret := _m.Called(_a0)

	if len(ret) == 0 {
		panic("no return value specified for GetCustomers")
	}

	var r0 []*storage.Customer
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context) ([]*storage.Customer, error)); ok {
		return rf(_a0)
	}
	if rf, ok := ret.Get(0).(func(context.Context) []*storage.Customer); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*storage.Customer)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Storage_GetCustomers_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetCustomers'
type Storage_GetCustomers_Call struct {
	*mock.Call
}

// GetCustomers is a helper method to define mock.On call
//   - _a0 context.Context
func (_e *Storage_Expecter) GetCustomers(_a0 interface{}) *Storage_GetCustomers_Call {
	return &Storage_GetCustomers_Call{Call: _e.mock.On("GetCustomers", _a0)}
}

func (_c *Storage_GetCustomers_Call) Run(run func(_a0 context.Context)) *Storage_GetCustomers_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context))
	})
	return _c
}

func (_c *Storage_GetCustomers_Call) Return(_a0 []*storage.Customer, _a1 error) *Storage_GetCustomers_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Storage_GetCustomers_Call) RunAndReturn(run func(context.Context) ([]*storage.Customer, error)) *Storage_GetCustomers_Call {
	_c.Call.Return(run)
	return _c
}

// GetPawn provides a mock function with given fields: _a0, _a1
func (_m *Storage) GetPawn(_a0 context.Context, _a1 storage.GetPawnParams) (*storage.Pawn, error) {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for GetPawn")
	}

	var r0 *storage.Pawn
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, storage.GetPawnParams) (*storage.Pawn, error)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, storage.GetPawnParams) *storage.Pawn); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*storage.Pawn)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, storage.GetPawnParams) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Storage_GetPawn_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetPawn'
type Storage_GetPawn_Call struct {
	*mock.Call
}

// GetPawn is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 storage.GetPawnParams
func (_e *Storage_Expecter) GetPawn(_a0 interface{}, _a1 interface{}) *Storage_GetPawn_Call {
	return &Storage_GetPawn_Call{Call: _e.mock.On("GetPawn", _a0, _a1)}
}

func (_c *Storage_GetPawn_Call) Run(run func(_a0 context.Context, _a1 storage.GetPawnParams)) *Storage_GetPawn_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(storage.GetPawnParams))
	})
	return _c
}

func (_c *Storage_GetPawn_Call) Return(_a0 *storage.Pawn, _a1 error) *Storage_GetPawn_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Storage_GetPawn_Call) RunAndReturn(run func(context.Context, storage.GetPawnParams) (*storage.Pawn, error)) *Storage_GetPawn_Call {
	_c.Call.Return(run)
	return _c
}

// GetPawnItemsByPawnID provides a mock function with given fields: _a0, _a1
func (_m *Storage) GetPawnItemsByPawnID(_a0 context.Context, _a1 string) ([]*storage.PawnItem, error) {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for GetPawnItemsByPawnID")
	}

	var r0 []*storage.PawnItem
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) ([]*storage.PawnItem, error)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) []*storage.PawnItem); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*storage.PawnItem)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Storage_GetPawnItemsByPawnID_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetPawnItemsByPawnID'
type Storage_GetPawnItemsByPawnID_Call struct {
	*mock.Call
}

// GetPawnItemsByPawnID is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 string
func (_e *Storage_Expecter) GetPawnItemsByPawnID(_a0 interface{}, _a1 interface{}) *Storage_GetPawnItemsByPawnID_Call {
	return &Storage_GetPawnItemsByPawnID_Call{Call: _e.mock.On("GetPawnItemsByPawnID", _a0, _a1)}
}

func (_c *Storage_GetPawnItemsByPawnID_Call) Run(run func(_a0 context.Context, _a1 string)) *Storage_GetPawnItemsByPawnID_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *Storage_GetPawnItemsByPawnID_Call) Return(_a0 []*storage.PawnItem, _a1 error) *Storage_GetPawnItemsByPawnID_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Storage_GetPawnItemsByPawnID_Call) RunAndReturn(run func(context.Context, string) ([]*storage.PawnItem, error)) *Storage_GetPawnItemsByPawnID_Call {
	_c.Call.Return(run)
	return _c
}

// GetPawns provides a mock function with given fields: _a0, _a1
func (_m *Storage) GetPawns(_a0 context.Context, _a1 string) ([]*storage.GetPawnsRow, error) {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for GetPawns")
	}

	var r0 []*storage.GetPawnsRow
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) ([]*storage.GetPawnsRow, error)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) []*storage.GetPawnsRow); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*storage.GetPawnsRow)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Storage_GetPawns_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetPawns'
type Storage_GetPawns_Call struct {
	*mock.Call
}

// GetPawns is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 string
func (_e *Storage_Expecter) GetPawns(_a0 interface{}, _a1 interface{}) *Storage_GetPawns_Call {
	return &Storage_GetPawns_Call{Call: _e.mock.On("GetPawns", _a0, _a1)}
}

func (_c *Storage_GetPawns_Call) Run(run func(_a0 context.Context, _a1 string)) *Storage_GetPawns_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *Storage_GetPawns_Call) Return(_a0 []*storage.GetPawnsRow, _a1 error) *Storage_GetPawns_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Storage_GetPawns_Call) RunAndReturn(run func(context.Context, string) ([]*storage.GetPawnsRow, error)) *Storage_GetPawns_Call {
	_c.Call.Return(run)
	return _c
}

// NewStorage creates a new instance of Storage. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewStorage(t interface {
	mock.TestingT
	Cleanup(func())
}) *Storage {
	mock := &Storage{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
